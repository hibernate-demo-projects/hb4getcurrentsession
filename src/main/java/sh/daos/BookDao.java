package sh.daos;

import org.hibernate.Session;

import sh.entities.Book;
import sh.util.HbUtil;

public class BookDao {
	/*
	 * Create Session object From Session Factory for every new Query via method getCurrentSession().
	 */

	/*
	 * Find Book By Id
	 */
	public Book findBookById(int id) {
		// Session object is created by getCurrentSession so there is no need to close it explicitly
		Session session = HbUtil.getCurrentSession();
		return session.find(Book.class, id);
	}
}