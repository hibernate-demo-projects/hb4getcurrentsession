package sh.main;

import sh.daos.BookDao;
import sh.entities.Book;
import sh.util.HbUtil;

public class Hb4Main {

	public static void main(String[] args) {
		BookDao bookDao = new BookDao();

		/*
		 * Transaction is compulsory if session object is obtained from
		 * getCurrentSessionM() method.
		 */
		try {
			HbUtil.beginTransaction();

			Book book = bookDao.findBookById(11);

			HbUtil.commitTransaction();

			System.out.println(book);

		} catch (Exception e) {
			HbUtil.rollbackTransaction();
			e.printStackTrace();
		}
	}
}
